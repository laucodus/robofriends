import React from 'react';
import Card from './Card';

const CardList = ({ robots }) => {
  // if(true){
  //   throw new Error('Fuck!')
  // }
  return robots.length ? (
    <div>
      {
        robots.map((user, i) => {
          return (
            <Card
              key={robots[i].id}
              id={robots[i].id}
              name={robots[i].name}
              email={robots[i].email}
            />
          );
        })
      }
    </div>
  ) : <h1>No robots found...</h1>
}

export default CardList;
