export const robots = [
  {
    id: 1,
    name: 'Taylor Swift',
    username: 'Taylor',
    email: 'taylor.swift@gmail.com'
  },
  {
    id: 2,
    name: 'Britney Spears',
    username: 'Britney',
    email: 'britney.spears@gmail.com'
  },
  {
    id: 3,
    name: 'Sony Moore',
    username: 'Skrillex',
    email: 'sony.moore@gmail.com'
  },
  {
    id: 4,
    name: 'Drei Hasselnuse',
    username: 'Drake',
    email: 'drei.hasselnuse@gmail.com'
  },
  {
    id: 5,
    name: 'Tom Smith',
    username: 'Tom',
    email: 'tom.smith@gmail.com'
  },
  {
    id: 6,
    name: 'David Beckhem',
    username: 'Davidx',
    email: 'david.beckhem@gmail.com'
  },
];
